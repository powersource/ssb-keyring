const test = require('tape')
const ssbKeys = require('ssb-keys')
const { directMessageKey, poBoxKey } = require('ssb-private-group-keys')
const URI = require('ssb-uri2')

const { tmpPath, Group, POBox, formats } = require('./helpers')
const Keyring = require('..')

test('keyring.encryptionKeys', async t => {
  const keyring = await Keyring(tmpPath())

  const mySign1 = ssbKeys.generate()
  const mySign2 = ssbKeys.generate(null, null, 'buttwoo-v1')
  const poBox = POBox()
  const group = Group()

  await keyring.dm.addFromSSBKeys(mySign1)
  await keyring.dm.addFromSSBKeys(mySign2)
  await keyring.poBox.add(poBox.id, { key: poBox.key })
  await keyring.group.add(group.id, { key: group.key, root: group.root })

  let recps // eslint-disable-line

  t.test('> self keys (dm-self)', t => {
    recps = [mySign1.id]
    const keys = keyring.encryptionKeys(mySign1.id, recps)
    t.deepEqual(
      keys,
      [keyring.self.get()],
      'self keyInfo corrent'
    )

    recps = [mySign1.id]
    t.deepEqual(keyring.encryptionKeys(mySign2.id, recps), keys, 'all my feeds have same selfKey')

    recps = [URI.fromFeedSigil(mySign1.id)]
    t.deepEqual(keyring.encryptionKeys(mySign2.id, recps), keys, 'all my feeds have same selfKey (URI)')

    // recps = [mySign1.id, mySign2.id]
    // t.deepEqual(keyring.encryptionKeys(mySign1.id, recps), keys, 'redundencies are removed')

    t.end()
  })

  t.test('> other keys (dm-other)', t => {
    formats.forEach(format => {
      const otherSign = ssbKeys.generate(null, null, format)
      recps = [otherSign.id]

      t.deepEqual(
        keyring.encryptionKeys(mySign1.id, recps),
        [directMessageKey.easy(mySign1)(otherSign.id)],
        `sharedDM : classic x ${format}`
      )
    })

    formats.forEach(format => {
      const otherSign = ssbKeys.generate(null, null, format)
      recps = [otherSign.id]

      t.deepEqual(
        keyring.encryptionKeys(mySign2.id, recps),
        [directMessageKey.easy(mySign2)(otherSign.id)],
        `sharedDM : buttwoo-v1 x ${format}`
      )
    })

    t.end()
  })

  t.test('> P.O. box keys', t => {
    recps = [poBox.id]

    const keys = keyring.encryptionKeys(mySign1.id, recps)
    t.deepEqual(
      keys,
      [poBoxKey.easy(mySign1)(poBox.id)],
      'poBox keyInfo correct'
    )

    // recps = [poBox.id, poBox.id]
    // t.deepEqual(keyring.encryptionKeys(mySign1.id, recps), keys, 'redundencies are removed')

    t.end()
  })

  t.test('> Group keys', t => {
    recps = [group.id]

    const keys = keyring.encryptionKeys(mySign1.id, recps)
    t.deepEqual(
      keys,
      [keyring.group.get(group.id)],
      'group keyInfo correct'
    )

    // recps = [poBox.id, poBox.id]
    // t.deepEqual(keyring.encryptionKeys(mySign1.id, recps), keys, 'redundencies are removed')

    recps = [Group().id]
    t.deepEqual(keyring.encryptionKeys(mySign1.id, recps), [null], 'unknown groupId => null')
    // TODO - or should it throw?

    t.end()
  })

  /* everything together */
  const otherSign = ssbKeys.generate()
  recps = [group.id, otherSign.id, mySign1.id, poBox.id]
  t.deepEqual(
    keyring.encryptionKeys(mySign1.id, recps),
    [
      keyring.group.get(group.id),
      directMessageKey.easy(mySign1)(otherSign.id),
      keyring.self.get(),
      poBoxKey.easy(mySign1)(poBox.id)
    ],
    'multiple different recps group keyInfo correct'
  )

  t.end()
})
