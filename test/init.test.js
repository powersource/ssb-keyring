const test = require('tape')

const keyRing = require('../')
const { tmpPath } = require('./helpers')

test('init / close', t => {
  t.plan(4)

  keyRing(tmpPath(), (err, keys) => {
    t.error(err, 'callback init')
    keys.close(err => t.error(err, 'callback close'))
  })

  keyRing(tmpPath())
    .catch(t.error)
    .then(keys => {
      t.ok(keys, 'promise init')

      keys.close()
        .catch(t.error)
        .then(() => t.pass('promise close'))
    })
})
