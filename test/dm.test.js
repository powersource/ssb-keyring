const test = require('tape')
const ssbKeys = require('ssb-keys')
const ssbURI = require('ssb-uri2')
const { DHKeys } = require('ssb-private-group-keys')

const Keyring = require('..')
const { tmpPath, formats } = require('./helpers')

test('keyring.dm', async t => {
  formats.forEach(format => {
    t.test(`> ${format} keys`, t => testKeys(t, format))
  })

  async function testKeys (t, format) {
    const keys = ssbKeys.generate(null, null, format)
    const id = keys.id
    const otherId = format === 'classic' ? ssbURI.fromFeedSigil(id) : null
    console.log(id)

    const path = tmpPath()
    let keyring = await Keyring(path)
    await keyring.dm.addFromSSBKeys(keys)
      .then(res => t.equal(res, undefined, 'keyring.dm.addFromSSBKeys'))
      .catch(err => t.error(err, 'keyring.dm.addFromSSBKeys'))

    t.true(keyring.dm.has(id), 'keyring.dm.has')
    if (otherId) t.true(keyring.dm.has(otherId), 'keyring.dm.has (URI)')

    const expected = {
      ...new DHKeys(keys, { fromEd25519: true }).toBuffer(), // { public, secret }
      scheme: 'envelope-id-based-dm-converted-ed25519'
    }
    t.deepEqual(keyring.dm.get(id), expected, 'keyring.dm.get')
    if (otherId) t.deepEqual(keyring.dm.get(otherId), expected, 'keyring.dm.get (URI)')

    t.deepEqual(keyring.dm.list(), [id], 'keyring.dm.list')

    await keyring.close()
    keyring = await Keyring(path)
    t.deepEqual(keyring.dm.get(id), expected, '...persists')
    if (otherId) t.deepEqual(keyring.dm.get(otherId), expected, '...persists (URI)')

    await keyring.close()
    t.end()
  }
})

// write test with add, no await + close
