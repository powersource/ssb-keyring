const test = require('tape')
const ssbKeys = require('ssb-keys')
const { directMessageKey, poBoxKey } = require('ssb-private-group-keys')

const { tmpPath, Group, POBox, formats } = require('./helpers')
const Keyring = require('..')

const areEqual = (t) => (actual, expected, message) => {
  t.deepEqual(actual, expected, message)

  /* activeate for easier diff'ing */
  // t.deepEqual(Object.keys(actual).sort(), Object.keys(expected).sort(), 'same result keys')
  // for (const key in expected) {
  //   t.deepEqual(actual[key], expected[key], 'result.' + key)
  // }
}

test('keyring.decryptionKeys', { objectPrintDepth: 9 }, async t => {
  const keyring = await Keyring(tmpPath())

  const mySign1 = ssbKeys.generate()
  const mySign2 = ssbKeys.generate(null, null, 'buttwoo-v1')
  const poBox = POBox()
  const group = Group()

  await keyring.dm.addFromSSBKeys(mySign1)
  await keyring.dm.addFromSSBKeys(mySign2)
  await keyring.poBox.add(poBox.id, { key: poBox.key })
  await keyring.group.add(group.id, { key: group.key, root: group.root })

  t.test('> author is me', t => {
    const signKeys = [mySign1, mySign2]

    signKeys.forEach(signKey => {
      const authorId = signKey.id

      areEqual(t)(
        keyring.decryptionKeys(authorId),
        {
          group: keyring.group.list().map(groupId => keyring.group.get(groupId)),
          poBox: [
            poBoxKey.easy(signKey)(poBox.id)
          ],
          self: [keyring.self.get()],
          dm: [] // NONE, they're me and will use self-key
        },
        'author: ' + authorId
      )
    })

    t.end()
  })

  t.test('> author is other', t => {
    formats.forEach(format => {
      const otherSign = ssbKeys.generate(null, null, format)
      const authorId = otherSign.id

      areEqual(t)(
        keyring.decryptionKeys(authorId),
        {
          group: keyring.group.list().map(groupId => keyring.group.get(groupId)),
          poBox: [
            poBoxKey.easy(otherSign)(poBox.id)
            // cheating, this isn't how I could derive, but keys are equivalent
          ],
          self: [], // NONE, they're other!
          dm: [
            directMessageKey.easy(mySign1)(authorId),
            directMessageKey.easy(mySign2)(authorId)
          ]
        },
        authorId
      )
    })

    t.end()
  })

  t.end()
})
