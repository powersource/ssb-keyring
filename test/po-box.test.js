/* eslint-disable camelcase */

const test = require('tape')

const Keyring = require('../')
const { tmpPath, POBox } = require('./helpers')

test('keyring.poBox', async t => {
  const path = tmpPath()
  let keyring = await Keyring(path)

  let DESCRIPTION
  const { id: poBoxId, key } = POBox()

  /* keys.poBox.add(poBoxId, keyInfo, cb) */
  DESCRIPTION = 'poBox.add, bad poBoxId => error'
  await keyring.poBox.add('junk', { key: POBox() })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected a poBoxId/, DESCRIPTION)) // ✓

  DESCRIPTION = 'poBox.add, bad info.key => error'
  await keyring.poBox.add(poBoxId, { key: 'junk' })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected buffer of length 32/, DESCRIPTION)) // ✓

  // TODO associate with groupId?
  // DESCRIPTION = 'poBox.add, bad info.root => error'
  // await keyring.poBox.add(Group().id, { key: GroupKey(), root: 'dog' })
  //   .then(res => t.fail(DESCRIPTION))
  //   .catch(err => t.match(err && err.message, /expected info.root to be MsgId/, DESCRIPTION)) // ✓

  DESCRIPTION = 'poBox.add, works!'
  const info = { key }
  await keyring.poBox.add(poBoxId, info)
    .then(res => t.pass(DESCRIPTION)) // ✓
    .catch(err => t.fail(err, DESCRIPTION))

  DESCRIPTION = 'poBox.add, same key again => error'
  await keyring.poBox.add(poBoxId, info)
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /already registered/, DESCRIPTION)) // ✓

  /* keys.poBox.get(poBoxId) */
  DESCRIPTION = 'poBox.get'
  const expected = info
  t.deepEqual(keyring.poBox.get(poBoxId), expected, DESCRIPTION)
  t.deepEqual(keyring.poBox.get(POBox().id), undefined, DESCRIPTION + ' (unknown poBoxId)')

  DESCRIPTION = '...persists'
  await keyring.close()
  keyring = await Keyring(path)
  t.deepEqual(keyring.poBox.get(poBoxId), expected, DESCRIPTION)

  /* keys.poBox.has(poBoxId) */
  DESCRIPTION = 'poBox.has'
  t.true(keyring.poBox.has(poBoxId), DESCRIPTION)
  t.false(keyring.poBox.has(POBox().id), DESCRIPTION + ' (unknown poBoxId)')

  /* keys.poBox.list() */
  DESCRIPTION = 'poBox.list'
  t.deepEqual(keyring.poBox.list(), [poBoxId], DESCRIPTION)

  keyring.close()
  t.end()
})
