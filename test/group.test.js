const test = require('tape')
const { keySchemes } = require('private-group-spec')

const Keyring = require('../')
const { tmpPath, GroupKey, GroupId, MsgId } = require('./helpers')

test('keyring.group', async t => {
  const path = tmpPath()
  let keyring = await Keyring(path)
  let DESCRIPTION

  /* keys.group.add(groupId, keyInfo, cb) */
  DESCRIPTION = 'group.add, bad groupId => error'
  await keyring.group.add('junk', { key: GroupKey(), root: MsgId() })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected a groupId/, DESCRIPTION)) // ✓

  DESCRIPTION = 'group.add, bad info.key => error'
  await keyring.group.add(GroupId(), { key: 'junk', root: MsgId() })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected buffer of length 32/, DESCRIPTION)) // ✓

  DESCRIPTION = 'group.add, bad info.root => error'
  await keyring.group.add(GroupId(), { key: GroupKey(), root: 'dog' })
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /expected info.root to be MsgId/, DESCRIPTION)) // ✓

  const groupId = GroupId()
  const info = { key: GroupKey(), root: MsgId() }

  DESCRIPTION = 'group.add, works!'
  await keyring.group.add(groupId, info)
    .then(res => t.pass(DESCRIPTION)) // ✓
    .catch(err => t.fail(err, DESCRIPTION))

  DESCRIPTION = 'group.add, same key again => error'
  await keyring.group.add(groupId, info)
    .then(res => t.fail(DESCRIPTION))
    .catch(err => t.match(err && err.message, /already registered/, DESCRIPTION)) // ✓

  /* keys.group.get(groupId) */
  DESCRIPTION = 'group.get'
  const expected = { ...info, scheme: keySchemes.private_group }
  t.deepEqual(keyring.group.get(groupId), expected, DESCRIPTION)
  t.deepEqual(keyring.group.get(GroupId()), undefined, DESCRIPTION + ' (unknown groupId)')

  DESCRIPTION = '...persists'
  await keyring.close()
  keyring = await Keyring(path)
  t.deepEqual(keyring.group.get(groupId), expected, DESCRIPTION)

  /* keys.group.has(groupId) */
  DESCRIPTION = 'group.has'
  t.true(keyring.group.has(groupId), DESCRIPTION)
  t.false(keyring.group.has(GroupId()), DESCRIPTION + ' (unknown groupId)')

  /* keys.group.list() */
  DESCRIPTION = 'group.list'
  t.deepEqual(keyring.group.list(), [groupId], DESCRIPTION)

  keyring.close()
  t.end()
})
