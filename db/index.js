const mkdirp = require('mkdirp')
const Level = require('level')
const charwise = require('charwise')

const infoEncoding = require('./info-encoding')

module.exports = function db (path) {
  if (typeof window === 'undefined') { // not in a browser
    mkdirp.sync(path)
  }

  return Level(path, {
    keyEncoding: charwise,
    valueEncoding: infoEncoding
  })
}
