/* eslint-disable brace-style */

const { DHKeys } = require('ssb-private-group-keys')
const { scheme } = require('private-group-spec/key-schemes.json')
const { promisify } = require('util')

const db = require('./db')

const Self = require('./models/self')
const DM = require('./models/dm')
const Group = require('./models/group')
const POBox = require('./models/po-box')

const { isFeed, isPOBox, isGroup } = require('./util')

module.exports = function Keyring (path, cb) {
  if (cb === undefined) return promisify(Keyring)(path)
  const level = db(path)

  const self = Self(level)
  const dm = DM(level)
  const group = Group(level)
  const poBox = POBox(level, dm)

  const api = {
    self: {
      set (info, cb) {
        if (cb === undefined) return promisify(api.self.set)(info)

        self.set(info, cb)
      },
      get: self.get
    },
    dm: {
      // add: directMessage.add,
      addFromSSBKeys (ssbSignKeys, cb) {
        if (cb === undefined) return promisify(api.dm.addFromSSBKeys)(ssbSignKeys)

        const dh = new DHKeys(ssbSignKeys, { format: 0, fromEd25519: true }).toBuffer()
        const info = {
          public: dh.public,
          secret: dh.secret,
          scheme: scheme.feed_id_dm
        }
        dm.add(ssbSignKeys.id, info, cb)
      },
      has: dm.has,
      get: dm.get,
      list: dm.list
    },
    group: {
      add (groupId, info, cb) {
        if (cb === undefined) return promisify(api.group.add)(groupId, info)

        group.add(groupId, info, cb)
      },
      has: group.has,
      get: group.get,
      list: group.list
    },
    poBox: {
      add (poBoxId, info, cb) {
        if (cb === undefined) return promisify(api.poBox.add)(poBoxId, info)

        poBox.add(poBoxId, info, cb)
      },
      has: poBox.has,
      get: poBox.get,
      list: poBox.list
    },
    encryptionKeys (authorId, recps) {
      return recps.map(recp => {
        if (isFeed(recp)) {
          return dm.has(recp)
            ? self.get()
            : dm.encryptionKey(authorId, recp)
        }
        else if (isGroup(recp)) return group.has(recp) ? group.get(recp) : null
        else if (isPOBox(recp)) return poBox.encryptionKey(authorId, recp)
        else return null
        // TODO
        // - should fail return null (this would make keys that crash encelope-js ??
        // - dedup identical keys?
      })
    },
    decryptionKeys (authorId) {
      const isMe = dm.has(authorId)
      return {
        self: isMe ? [self.get()] : [],
        group: group.list().map(group.get),
        dm: isMe ? [] : dm.decryptionKeys(authorId),
        poBox: poBox.decryptionKeys(authorId)
      }
    },

    close: level.close.bind(level)
  }

  /* loads persisted states into cache */
  group.load(err => {
    if (err) return cb(err)
    self.load(err => {
      if (err) return cb(err)
      dm.load(err => {
        if (err) return cb(err)
        poBox.load(err => {
          if (err) return cb(err)
          cb(null, api)
        })
      })
    })
  })
  // TODO
  // - parallelize
  // - close level if there was an error!
}
