const pull = require('pull-stream')
const { read } = require('pull-level')
const na = require('sodium-universal')
const { DHKeys, poBoxKey } = require('ssb-private-group-keys')
const bfe = require('ssb-bfe')

const { toBuffer, isPOBoxId } = require('../util')

const POBOX = 'pobox'

module.exports = function POBox (db, dm) {
  let cache // Map: groupId => group.info

  const api = {
    load (cb) {
      readPersisted((err, map) => {
        if (err) return cb(err)
        cache = map
        cb(null)
      })
    },

    add (poBoxId, info, cb) {
      if (!cache) throw new Error('keyring not ready')
      if (cache.has(poBoxId)) return cb(new Error(`already registered poBoxId ${poBoxId}, cannot register twice`))
      if (!isPOBoxId(poBoxId)) return cb(new Error(`expected a poBoxId, got ${poBoxId}`))
      if (!info.key) return cb(new Error('expected info.key'))
      // TODO <<< store public/secret keys in BFE format, along with ID in BFE format ???

      try {
        // convert to 32 Byte buffer
        info.key = toBuffer(info.key, na.crypto_scalarmult_SCALARBYTES)
      } catch (e) { return cb(e) }

      cache.set(poBoxId, info)
      db.put([POBOX, poBoxId, Date.now()], info, cb)
      // TODO store entity poBox is associated with too?
      // - groupId (so only reindex the group lobby feed?
      // - fusionId
    },
    has (poboxId) {
      if (!cache) throw new Error('keyring not ready')
      return cache.has(poboxId)
    },
    get (poboxId) {
      if (!cache) throw new Error('keyring not ready')
      return cache.get(poboxId)
    },
    list () {
      if (!cache) throw new Error('keyring not ready')
      return Array.from(cache.keys())
    },

    encryptionKey (authorId, poBoxId) {
      if (!dm.has(authorId)) {
        throw new Error(`No keypair registered for ${authorId}, cannot get shared P.O. Box key`)
      }

      const x = {
        dh: new DHKeys(dm.get(authorId), { format: 0 }).toBFE(),
        id: bfe.encode(authorId)
      }

      const yId = bfe.encode(poBoxId)
      const y = {
        dh: new DHKeys({ public: yId.slice(2) }, { format: 1 }).toBFE(),
        id: yId
      }

      return poBoxKey(
        x.dh.secret, x.dh.public, x.id,
        y.dh.public, y.id
      )
    },

    decryptionKey (authorId, poBoxId) {
      const keyInfo = api.get(poBoxId)

      const xId = bfe.encode(poBoxId)
      const x = {
        dh: {
          public: Buffer.concat([
            bfe.toTF('encryption-key', 'box2-pobox-dh'),
            xId.slice(2)
          ]),
          secret: Buffer.concat([
            bfe.toTF('encryption-key', 'box2-pobox-dh'),
            keyInfo.key
          ])
        },
        id: xId
      }

      const yId = bfe.encode(authorId)
      const y = {
        dh: new DHKeys({ public: yId.slice(2) }, { fromEd25519: true }).toBFE(),
        id: yId
      }

      return poBoxKey(
        x.dh.secret, x.dh.public, x.id,
        y.dh.public, y.id
      )
    },
    decryptionKeys (authorId) {
      return api.list().map(poBoxId => api.decryptionKey(authorId, poBoxId))
    }
  }

  return api

  function readPersisted (cb) {
    pull(
      read(db, {
        lt: [POBOX + '~', undefined, undefined], // "group~" is just above "group" in charwise sort
        gt: [POBOX, null, null]
      }),
      pull.map(({ key, value: info }) => {
        const [_, poBoxId, createdAt] = key // eslint-disable-line
        return [poBoxId, info]
      }),
      pull.collect((err, pairs) => {
        if (err) return cb(err)
        cb(null, new Map(pairs))
      })
    )
  }
}
