const pull = require('pull-stream')
const { read } = require('pull-level')
const { keySchemes } = require('private-group-spec')
const { isMsg, isCloakedMsg: isGroup } = require('ssb-ref')
const na = require('sodium-universal')

const { toBuffer } = require('../util')

const GROUP = 'group'

module.exports = function Group (db) {
  let cache = new Map() // Map: groupId => group.info

  return {
    load (cb) {
      readPersisted((err, pairs) => {
        if (err) return cb(err)
        cache = new Map(pairs)
        cb(null)
      })
    },

    add (groupId, info, cb) {
      if (!cache) throw new Error('keyring not ready')
      if (cache.has(groupId)) return cb(new Error(`group ${groupId} already registered, cannot register twice`))
      if (!isGroup(groupId)) return cb(new Error(`expected a groupId, got ${groupId}`))
      // TODO check info.scheme is valid in some way?
      if (!info.scheme) info.scheme = keySchemes.private_group
      if (info.root && !isMsg(info.root)) return cb(new Error(`expected info.root to be MsgId, got ${info.root}`))

      try {
        // convert to 32 Byte buffer
        info.key = toBuffer(info.key, na.crypto_secretbox_KEYBYTES)
      } catch (e) { return cb(e) }

      cache.set(groupId, info)
      db.put([GROUP, groupId, Date.now()], info, cb)
    },
    has (groupId) {
      if (!cache) throw new Error('keyring not ready')
      return cache.has(groupId)
    },
    get (groupId) {
      if (!cache) throw new Error('keyring not ready')
      return cache.get(groupId)
    },
    list () {
      if (!cache) throw new Error('keyring not ready')
      return Array.from(cache.keys())
    }
  }

  function readPersisted (cb) {
    pull(
      read(db, {
        gt: [GROUP, null, null],
        lt: [GROUP + '~', undefined, undefined]
      }),
      pull.map(({ key, value: info }) => {
        const [_, groupId, registeredAt] = key // eslint-disable-line
        return [groupId, info]
      }),
      pull.collect((err, pairs) => {
        if (err) return cb(err)
        cb(null, pairs)
      })
    )
  }
}
