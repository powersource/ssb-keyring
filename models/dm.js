const pull = require('pull-stream')
const { read } = require('pull-level')
const { DHKeys, directMessageKey } = require('ssb-private-group-keys')
const bfe = require('ssb-bfe')
const URI = require('ssb-uri2')

const normalise = (authorId) => authorId.startsWith('@')
  ? URI.fromFeedSigil(authorId)
  : authorId
const denormalise = authorId => URI.isClassicFeedSSBURI(authorId)
  ? URI.toFeedSigil(authorId)
  : authorId

const DM_KEYS = 'dm'

module.exports = function DirectMessage (db) {
  let cache = new Map() // Map: authorId => { public, private }

  const api = {
    load (cb) {
      readPersisted((err, pairs) => {
        if (err) return cb(err)
        cache = new Map(pairs)
        cb(null)
      })
    },

    add (authorId, info, cb) {
      if (!cache) throw new Error('keyring not ready')
      if (cache.has(authorId)) return console.warn(`ssb-keyring is ignoring dm.add for ${authorId} because it already exists`)

      if (!info) return cb(new Error(`${authorId} info is not defined`))
      if (!info.public) return cb(new Error(`${authorId} info.public key is not defined`))
      if (!info.secret) return cb(new Error(`${authorId} info.secret key is not defined`))
      if (!info.scheme) return cb(new Error(`${authorId} info.scheme is not defined`))

      authorId = normalise(authorId)
      cache.set(authorId, info)
      db.put([DM_KEYS, authorId, Date.now()], info, cb)
    },

    get (authorId) {
      return cache.get(normalise(authorId))
    },

    has (authorId) {
      return cache.has(normalise(authorId))
    },

    list () {
      return Array.from(cache.keys())
        .map(denormalise)
    },

    encryptionKey (authorId, feedId) {
      // here the authorId is one of our feed's ids, and feedId is a foreign feed
      authorId = normalise(authorId)
      if (!api.has(authorId)) {
        throw new Error(`No keypair registered for ${authorId}, cannot get shared DM key`)
      }
      if (api.has(feedId)) {
        throw new Error('Not allowed to get a shared DM Key for your own feedId')
      }

      const x = {
        dh: new DHKeys(api.get(authorId), { format: 0 }).toBFE(),
        id: bfe.encode(authorId)
      }

      const yId = bfe.encode(feedId)
      const y = {
        dh: new DHKeys({ public: yId.slice(2) }, { fromEd25519: true }).toBFE(),
        id: yId
      }

      return directMessageKey(
        x.dh.secret, x.dh.public, x.id,
        y.dh.public, y.id
      )
    },

    decryptionKey (authorId, feedId) {
      authorId = normalise(authorId)
      feedId = normalise(feedId)
      // here the authorId is foreign feeds id, and feedId is one of our feedIds
      if (!api.has(feedId)) {
        throw new Error(`No keypair registered for ${authorId}, cannot get shared DM key`)
      }
      if (api.has(authorId)) {
        throw new Error('Not allowed to get a shared DM Key for your own feedId')
      }

      const x = {
        dh: new DHKeys(api.get(feedId), { format: 0 }).toBFE(),
        id: bfe.encode(feedId)
      }

      const yId = bfe.encode(authorId)
      const y = {
        dh: new DHKeys({ public: yId.slice(2) }, { fromEd25519: true }).toBFE(),
        id: yId
      }

      return directMessageKey(
        x.dh.secret, x.dh.public, x.id,
        y.dh.public, y.id
      )
    },
    decryptionKeys (authorId) {
      authorId = normalise(authorId)
      return api.list().map(feedId => api.decryptionKey(authorId, feedId))
    }
  }
  return api

  function readPersisted (cb) {
    pull(
      read(db, {
        gt: [DM_KEYS, null, null],
        lt: [DM_KEYS + '~', undefined]
      }),
      pull.map(({ key, value: info }) => {
        return [normalise(key[1]), info]
      }),
      pull.collect((err, pairs) => {
        if (err) return cb(err)
        cb(null, pairs)
      })
    )
  }
}
