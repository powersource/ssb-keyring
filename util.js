const bfe = require('ssb-bfe')
const { isCloakedMsgId } = require('ssb-ref')
// const na = require('sodium-universal')

function toBuffer (thing, length) {
  const buf = Buffer.isBuffer(thing)
    ? thing
    : Buffer.from(thing, 'base64')

  if (length && buf.length !== length) {
    throw new Error(`expected buffer of length ${length} bytes, got ${buf.length}`)
  }

  return buf
}

const POBOX_TF = bfe.toTF('identity', 'po-box')
function isPOBoxId (id) {
  try {
    const bfeId = bfe.encode(id)

    return (
      bfeId.length === 34 && // 2 + na.crypto_scalarmult_SCALARBYTES = 34 bytes
      bfeId.slice(0, 2).equals(POBOX_TF)
    )
  } catch (err) {
    return false
  }
}

function isFeedId (str) {
  // TODO make this stronger?
  return str.match(/^(@|ssb:feed)/)
}

module.exports = {
  toBuffer,
  isPOBoxId,
  isPOBox: isPOBoxId,
  isGroupId: isCloakedMsgId,
  isGroup: isCloakedMsgId,
  isFeedId,
  isFeed: isFeedId,
  isBuffer: Buffer.isBuffer,
  isString: (str) => typeof str === 'string',
  isObject (obj) {
    if (obj === null) return false
    if (Buffer.isBuffer(obj)) return false
    return typeof obj === 'object'
  },
  isSameKey (A, B) {
    return toBuffer(A).equals(toBuffer(B))
  }
  // signKeysToEncryptionKeys
}
