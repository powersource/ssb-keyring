# ssb-keyring

A persistence store for encryption keys for scuttlebutt.
It's purpose is to make easy to answer box2 encryption/ decryption questions.

_This module was extracted from `ssb-tribes`_

## Usage

```js
const keyRing = require('ssb-keyring')
const ssbKeys = require('ssb-keys')

const me = ssbKeys.generate()

keyRing(`/tmp/keyring-demo-${Date.now()}`, (err, keys) => {
  keys.dm.addFromSSBKeys(me)

  const trialKeys = keys.decryptionKeys(otherFeedId)

  // ...use trialKeys with a decryption library
})
```

## API

This module is responsible for:
1. recording key details
    - persisting encryption keys (async)
    - persisting who has access to keys (async)
2. accessing keys
    - "which keys should I try when decrypting a message from feed X?"
    - "if I'm publishing a message to feed Y, and want to encrypt it to _these_ recps, which keys should I use?"

Overview (methods most people should use):

```js
/* SETUP */
keyRing(dbPath, cb)

/* REGISTERING */
keys.dm.addFromSSBKeys(ssbKeys, cb)
keys.poBox.add(poBoxId, keyInfo, cb)
keys.group.add(groupId, keyInfo, cb)

/* QUERYING */
keys.decryptionKeys(authorId)        // => [keyInfo]
keys.encryptionKeys(authorId, recps) // => [keyInfo]

keys.close(cb)
```

All querying methods are synchronous, so that encryption / decryption
never has to wait for IO to access keys.

All registering methods are:
- asynchronous
    - if provided a callback `cb` will callback
    - if not provided a callback, return a Promise
- can be used synchronously
    - all querying is done on an in-memory cache, which new items are added to
    - the only asynchronous aspect is for persistence

### `keyRing(path, cb)`

where
- `path` *String* to location your keys will be persisted to on disk
- `cb` *function* calls back with `(err, keys)` (where `keys` is the key-ring API)
    - if `cb` not provided, function returns a Promise

---

### `keys.dm.addFromSSBKeys(ssbKeys, cb)`

Takes some signing keys `ssbKeys` (made by `ssb-keys` or similar),
and adds them as keys you also use for encryption (by converting them from ed25519 to curve25519).

Adds keys that a particular feed you can write to uses for DMs to others
- `ssbKeys` *Object* containing ed25519 keys
    - `ssbKeys.public` *String* base64 encoded public part of the DM key
    - `ssbKeys.secret` | `ssbKeys.private` *String* base64 encoded secret/private part the DM keypair
- `cb` *function* callback with signature `(err)`

NOTE: this method establishes which feeds you author, which is relevant for:
- knowing how to encrypt messages to another feed or P.O. Box
- knowing when a message was authored by you - involves self-key encryption

### `keys.dm.has(feedId) => Boolean`

Find out whether a particular `feedId` has been registered.
If it has you can use it for encrypting to : self, other feedIds, P.O. Boxes

`feedId` can be a classic sigil (for classic feeds) or an SSB-URI.

### `keys.dm.get(feedId) => keyInfo`
### `keys.dm.list() => [feedId]`


---

### `keys.self.set(keyInfo, cb)`

Your keyring will always intialize and check if a self-key has been set.
You can use this method to over-ride that default.

- `keyInfo` *Object* contains symmetric key
    - `info.key` *String* base64 encoded symmetric key
    - `info.scheme` *string* (optional)
- `cb` *function* callback with signature `(err)`

### `keys.self.get() => keyInfo`

---

### `keys.group.add(groupId, info, cb)`

where
- `groupId` *String* a cloaked messageId which identifies the group
- `info` *Object*:
    - `info.key` *Buffer* - the group encryption key
    - `info.scheme` *String* - scheme of that encryption key (optional, there is only one option at the moment which we default to)
    - `info.root` *MessageId* the id of the `group/init` message

### `keys.group.has(groupId) => Boolean`
### `keys.group.get(groupId) => keyInfo`
### `keys.group.list() => [groupId]`

---

### `keys.poBox.add(poBoxId, info, cb)`

where
- `poBoxId` *String* is an SSB-URI for a P.O. Box
- `info` *Object*
    - `info.key` *Buffer* - the private part of a diffie-hellman key
    - `info.scheme` *String* the scheme associated with that key (currently optional)

### `keys.poBox.has(poBoxId) => Boolean`
### `keys.poBox.get(poBoxId) => keyInfo`
### `keys.poBox.list(poBoxId) => [poBoxId]`


### `keys.decryptionKeys(authorId) => trialKeys`

Find the keys you could use to try decrypting a message from `authorId`, where:
- `authorId` *String* a feedId (either sigil or URI based)
- `trialKeys` *Object* where:
    - `trialKeys.self` *Array* collection of self keyInfo (empty Array if authorId is not yours)
    - `trialKeys.group` *Array* collection of keyInfo for Private Groups
    - `trialKeys.dm` *Array* collection of DM keyInfo (empty Array if authorId is yours)
    - `trialKeys.poBox` *Array* collection of P.O. Box keyInfo

### `keys.encryptionKeys(authorId, recps) => [keyInfo]`

Map an array of recipients to keyInfo used to encrypt messages to those recipients, where:
- `authorId` *String* the id of the feed you're going to publish the message to (in sigil/ URI form)
- `recps` *Array* collection of recipients (feedId, groupId, poBoxId in sigil/ URI form)

NOTE:
1. if matching keyInfo cannot be found/ computed, no error is thrown
    - instead then that keyInfo will come back `null`
2. it is possible for duplicated of the same keyInfo to come back
    - e.g. if you put two `feedId` in recps which are both yours, then these will return the same self-key
    - this module does not de-duplicate keys

---

### `keys.close(cb)`

Closes the keyring database


---

## Questions

1. what do if `encryptionKeys(recps)` fails?
    - can fail if authorId is unknown, or if don't have a keyInfo for e.g. a group
    - options:
        - throw Error
        - return null 
        - **return [keyInfo, null, keyInfo]** << ✓ this one currently
           - relies on boxer to catch this
2. API is not really ready till you have recorded your DM + Own keys...
    - this is a footgun?
    - do some method error?
3. can we associate a PO Box with the lobby of a particular group?
    - want to be able to get a PO Box key and know what to re-index (minimally)
    - or do we just try the key on _everything_?
4. how does encryption/ decryption across meta-feeds
    - A chess app has a meta-feed with DMs for chess chat, are the recps:
        - [@chessFeedA, @chessFeedB]
        - [@rootFeedA, @rootFeedB]

## Future ideas

- track membership
- support having more than on self/ own key
- add "processors" for group messages:
    - `group/init`
    - `group/add-member`
- `keys.dm.add(feedId, dhKeys, cb)`
    - would allow signKeys !== encryptionKeys
    - the current scheme = 'envelope-id-based-dm-converted-ed25519' so we can't make add available unless it's another scheme??
- version the db (by filename)


